#pragma once
#include <iostream>
#include "dependecies/eigen3/Eigen/Dense"
#include <string>
#include <memory>
#include "iPotential.h"
/*-------------------------------------------------
Lattice(const int& Nx, const int& Ny, const int& Nz, const double& density, std::string const& typeoflattice,
		const double& temperature, std::string const& spinstate, const double& in_delta, const double& in_g);

Lattice(const int& Nx, const int& Ny, const double& density, std::string const& typeoflattice,
		const double& temperature, std::string const& spinstate, const double& in_delta, const double& in_g);

The Spin States of the lattice are: LS, HS, Random
The types of lattices are: square, triangular, kagome, cubic, fcc
in_delta=>ligand field splitting
in_g=> degeneracy ratio
particle coordinates, acceleration, velocities 
are stored in columns:
    0   1   2   3   4
|   x0  x1  x2        |
|   y0  y1  y2  ...   |
|   z0  z1  z3        |
nearest neighbours and next nearest neighbours 
are indexed such that
the first half is symmetric to the second half
this is useful when computing forces and energies, 
because we only need half of them


*/
class Lattice
{
public:
	enum TypeOfLattice
	{
		square, triangular, kagome, cubic, fcc
	};
	enum SpinState
	{
		LS, HS, Random
	};
private:
	TypeOfLattice m_TypeOfLattice;
	SpinState m_initSpinState; //initial spin state
	std::shared_ptr<iPotential> p_firstpotential; //first potential (pointer)
	std::shared_ptr<iPotential> p_secondpotential; //second potential (pointer)
	std::shared_ptr<iPotential> p_thirdpotential; //third potential (pointer)
	bool isCoorScaled; //at the beginning coordinates are unscaled
	bool isVelScaled; //at the beginning velocities are unscaled
 	int Nx; //number of unit cells in the x direction
	int Ny; //number of unit cells in the y direction
	int Nz; //number of unit cell in the z direction
	int Nm; //number of molecules
	int nf;//degrees of freedom
	double m = 1.0; //mass of particles
	int dim; //dimension of the lattice {2D:2, 3D:3}
	int NumNeig; //number of nearest neighbours
	int NumNextNeig; //number of next nearest neighbours
	double InitTemperature; //Initial temperature (Used for thermostat)
	double CurrTemperature; //current temperature of lattice (Used for thermostat)

	//to be used for first part of hamiltonian: Heff= 0.5(delta-T*ln(g))
	double delta; //ligand field splitting
	double g; //degeneracy
	double Heff; 

//Structural variables
public:
	int TotSpin;
	//matrices to store positions of molecules
	//at time t and t+h
	Eigen::MatrixXd rt;
	Eigen::MatrixXd rtph;
	//matrices to store velocities of molecules
	//at time t and t+h
	Eigen::MatrixXd vt;
	Eigen::MatrixXd vtph;
	//Matrices to store accelerations of molecules
	//at time: t-2h, t-h, t, t+h
	Eigen::MatrixXd atmh;
	Eigen::MatrixXd atm2h;
	Eigen::MatrixXd at;
	Eigen::MatrixXd atph;
	//Array to store nearest neighbours
	Eigen::ArrayXXi nneig;
	//Array to store next nearest neighbours
	Eigen::ArrayXXi nextnneig;
	//Simulation Box vectors
	Eigen::MatrixXd Ht;
	Eigen::MatrixXd Htph;
	//lattice volume
	double volt;
	double voltph;
	//Array to store spin of particles
	Eigen::ArrayXi spin;

	//total forces
	Eigen::MatrixXd totalForces;

//Thermodynamic variables
public:
	double Epotential; //potential energy
	double Ekinetic; //kinetic energy
	double Espin; //energy associated with spin states

//Hydrostatic variables
public:
	Eigen::MatrixXd Ptensor; //pressure tensor
	double pressure;
	double rdotf;

//constructor
public:
	Lattice(const int& Nx, const int& Ny, const int& Nz, const double& density, std::string const& typeoflattice,
		const double& temperature, std::string const& spinstate, const double& in_delta, const double& in_g);
	Lattice(const int& Nx, const int& Ny, const double& density, std::string const& typeoflattice,const double& temperature, 
		std::string const& spinstate, const double& in_delta, const double& in_g);

public:
	void SetNonCurrentMatricesToZero(); //used to set structural matrices to zero for t!=current time
	int GetNx();
	int GetNy();
	int GetNz();
	int GetNm();
	int GetNf();
	int GetDim();
	int GetNumNeig();
	int GetNumNextNeig();
	double GetInitTemperaure();
	std::string GetTypeOfLattice();
	std::string GetInitSpinState();
	void ChangeCurrTemperature(const double& NewTemperature);
	double GetHeff();
	double Getg();
	double GetDelta();
	//=============================================================================
	void SetPotential(std::shared_ptr<iPotential> p_potential);
	void CalculateTotalForces(); //this function alters totalForces, rdotf, Ptensor, Epotential
	//=============================================================================
	void PredictorStepCoordinates(const double& dt);
	void CorrectorStepCoordinates(const double& dt);
	void ScaleCoor(); //Scale coordinates
	void ScaleVel(); //Scale velocities
	void UnscaleCoor(); //Unscale coordinates
	void UnscaleVel(); //Unscale velocities
	void WrapCoort(); //wrap for rt
	void WrapCoortph(); //wrap for rtph

private:
	//type of lattice can be square, triangular, cubic, fcc, kagome
	//it has to be passed with those names
	TypeOfLattice Set3DType(std::string const& typeoflattice);
	TypeOfLattice Set2DType(std::string const& typeoflattice);
	
	void SetMatricesSizes();//set the size of matrices
	void SetLattice(const double& density);//set our lattice with a given symmetry

	//functions to set Lattices to a symmetry
	void SquareLattice(const double& density);
	void TriangularLattice(const double& density);
	void KagomeLattice(const double& density);
	void CubicLattice(const double& density);
	void FccLattice(const double& density);

	//function to set velocities
	void Set2DVelocities();
	void Set3DVelocities();

	//set spin of molecules
	SpinState TypeOfSpinState(std::string const& spinstate);
	void SetSpinState();

};
int mod(int a, int b);