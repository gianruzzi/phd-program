#include <iostream>
#include <cmath>
#include "Integrator.h"
#include "dependecies/eigen3/Eigen/Dense"

void PR4(Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& vt,
	const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh, const Eigen::MatrixXd& atm2h,
	const double& dt) {
	rtph = rt + dt * vt + (pow(dt, 2.0) / 24.0)*(19.0*at - 10.0*atmh + 3.0*atm2h);
}

void PV4(Eigen::MatrixXd& vtph, const Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt,
	const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh, const Eigen::MatrixXd& atm2h,
	const double& dt) {
	vtph = (rtph - rt)*(1.0 / dt) + (dt / 24.0)*(27.0*at - 22.0*atmh + 7.0*atm2h);

}

void CR4(Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& vt,
	const Eigen::MatrixXd& atph, const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh,
	const double& dt) {
	rtph = rt + dt * vt + (pow(dt, 2.0) / 24.0)*(3.0*atph + 10.0*at - 1.0*atmh);
}

void CV4(Eigen::MatrixXd& vtph, const Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt,
	const Eigen::MatrixXd& atph, const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh,
	const double& dt) {
	vtph = (rtph - rt)*(1.0 / dt) + (dt / 24.0)*(7.0*atph + 6.0*at - 1.0*atmh);
}

//have to use for each coordinate
void PredictorStep(Eigen::MatrixXd& rtph, Eigen::MatrixXd& rt, Eigen::MatrixXd& vtph, Eigen::MatrixXd& vt,
	Eigen::MatrixXd& at, Eigen::MatrixXd& atmh, Eigen::MatrixXd& atm2h, const double& dt) {
	PR4(rtph, rt, vt, at, atmh, atm2h, dt);
	PV4(vtph, rtph, rt, at, atmh, atm2h, dt);
	atm2h = atmh;
}

//each matrix contains the information for all particles and coordinates
//rows particles, columns coordinate
void CorrectorStep(Eigen::MatrixXd& rtph, Eigen::MatrixXd& rt, Eigen::MatrixXd& vt, Eigen::MatrixXd& vtph,
	Eigen::MatrixXd& atph, Eigen::MatrixXd& at, Eigen::MatrixXd& atmh, const double& dt) {
	CR4(rtph, rt, vt, atph, at, atmh, dt);
	CV4(vtph, rtph, rt, atph, at, atmh, dt);
	//prepare variables for next time step t+h=>t and so on
	rt = rtph;
	vt = vtph;
	atmh = at;
	at = atph;
}
//overload when using doubles instead of matrices
//======================================================================================================
void PR4(double& rtph, const double& rt, const double& vt,
	const double& at, const double& atmh, const double& atm2h,
	const double& dt) {
	rtph = rt + dt * vt + (pow(dt, 2.0) / 24.0)*(19.0*at - 10.0*atmh + 3.0*atm2h);
}

void PV4(double& vtph, const double& rtph, const double& rt,
	const double& at, const double& atmh, const double& atm2h,
	const double& dt) {
	vtph = (rtph - rt)*(1.0 / dt) + (dt / 24.0)*(27.0*at - 22.0*atmh + 7.0*atm2h);

}

void CR4(double& rtph, const double& rt, const double& vt,
	const double& atph, const double& at, const double& atmh,
	const double& dt) {
	rtph = rt + dt * vt + (pow(dt, 2.0) / 24.0)*(3.0*atph + 10.0*at - 1.0*atmh);
}

void CV4(double& vtph, const double& rtph, const double& rt,
	const double& atph, const double& at, const double& atmh,
	const double& dt) {
	vtph = (rtph - rt)*(1.0 / dt) + (dt / 24.0)*(7.0*atph + 6.0*at - 1.0*atmh);
}

//have to use for each coordinate
void PredictorStep(double& rtph, double& rt, double& vtph, double& vt,
	double& at, double& atmh, double& atm2h, const double& dt) {
	PR4(rtph, rt, vt, at, atmh, atm2h, dt);
	PV4(vtph, rtph, rt, at, atmh, atm2h, dt);
	atm2h = atmh;
}

//each matrix contains the information for all particles and coordinates
//rows particles, columns coordinate
void CorrectorStep(double& rtph, double& rt, double& vt, double& vtph,
	double& atph, double& at, double& atmh, const double& dt) {
	CR4(rtph, rt, vt, atph, at, atmh, dt);
	CV4(vtph, rtph, rt, atph, at, atmh, dt);
	//prepare variables for next time step t+h=>t and so on
	rt = rtph;
	vt = vtph;
	atmh = at;
	at = atph;
}