#pragma once
#include <string>
#include "dependecies/eigen3/Eigen/Dense"

class iPotential
{
public:
	virtual std::string GetName() = 0;
	virtual void CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor, double & rdotf, double & Epotential, const bool& velPtensor) = 0;
	virtual void CalculatePotentialEnergy(double & rdotf, double & Epotential) = 0;
};