#include <string>
#include <cmath>
#include "Lattice.h"
#include "Potential.h"
#include "dependecies/eigen3/Eigen/Dense"

//Elastic potential
//===========================================================================
template class Elastic<double>;
template class Elastic<Eigen::ArrayXd>;

template<class T>
Elastic<T>::Elastic(Lattice* Lat, const T & in_kijn, const T & in_kijnn, const Eigen::ArrayXd & in_radiusn, const Eigen::ArrayXd & in_radiusnn, const bool & ActiveNext)
{
	m_name = "Elastic potential";
	p_Lattice = Lat;
	kijn = in_kijn;
	kijnn = in_kijnn;
	radiusn = in_radiusn;
	radiusnn = in_radiusnn;  
	ActiveNextNearNeig = ActiveNext;
	NumNeig = Lat->GetNumNeig();
	NumNextNeig = Lat->GetNumNextNeig();
	Nm = Lat->GetNm();
	dim = Lat->GetDim();

}

template<class T>
std::string Elastic<T>::GetName()
{
	return m_name;
}

template<class T>
void Elastic<T>::LocalForce(const Eigen::ArrayXi& spin,const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int& currMol, const Eigen::ArrayXXi & neighbours,
	const int & NumNeig, const Eigen::ArrayXd & radius, const double & kij, Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor, 
	double & rdotf, double & Epotential)
{
	double modu;
	double requi;
	Eigen::VectorXd fij(dim);
	Eigen::VectorXd rij(dim);
	int j, k;
	for (j = 0; j < NumNeig / 2; j++)
	{
		k = neighbours(currMol, j);
		rij = rt.col(currMol) - rt.col(k);
		rij = rij.array() - (rij.array()).round();
		rij = Ht * rij;
		modu = rij.norm(); //modulus vector between balls
		requi = radiusn(spin(currMol)) + radiusn(spin(k));
		fij = (-kij * (modu - requi) / modu)*rij;
		force.col(currMol) += fij;
		force.col(k) += (-1.0)*fij;
		Ptensor += rij * fij.transpose();
		rdotf += fij.dot(rij);
		Epotential += 0.5*kij*pow(modu - requi, 2.0);
	}

}

template<class T>
void Elastic<T>::LocalForce(const Eigen::ArrayXi& spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int& currMol, const Eigen::ArrayXXi & neighbours,
	const int & NumNeig, const Eigen::ArrayXd & radius, const Eigen::ArrayXd& kij, Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor,
	double & rdotf, double & Epotential)
{
	double modu;
	double requi;
	Eigen::VectorXd fij(dim);
	Eigen::VectorXd rij(dim);
	int j, k;
	for (j = 0; j < NumNeig / 2; j++)
	{
		k = neighbours(currMol, j);
		rij = rt.col(currMol) - rt.col(k);
		rij = rij.array() - (rij.array()).round();
		rij = Ht * rij;
		modu = rij.norm(); //modulus vector between balls
		requi = radiusn(spin(currMol)) + radiusn(spin(k));
		fij = (-kij(j) * (modu - requi) / modu)*rij;
		force.col(currMol) += fij;
		force.col(k) += (-1.0)*fij;
		Ptensor += rij * fij.transpose();
		rdotf += fij.dot(rij);
		Epotential += 0.5*kij(j)*pow(modu - requi, 2.0);
	}
}


//force, Ptensor, rdotf, and Epotential are set to zero before calculating
//here we use rtph, vtph and Htph, they must be scaled
template<class T>
void Elastic<T>::CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor,
	double & rdotf, double & Epotential, const bool& velPtensor)
{
	force.setZero();
	Ptensor.setZero();
	rdotf = 0.0;
	Epotential = 0.0;
	int i;
	if (ActiveNextNearNeig)
	{
		if (velPtensor)
		{
			Eigen::MatrixXd vtr(dim, Nm);
			vtr = (p_Lattice->Htph) * (p_Lattice->vtph); //unscale velocities
			for (i = 0; i < Nm; i++)
			{
				Ptensor += (vtr.col(i))*(vtr.col(i).transpose());
				LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nneig, NumNeig, radiusn, kijn, force, Ptensor,
					rdotf, Epotential);
				LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nextnneig, NumNextNeig, radiusnn, kijnn, force, Ptensor,
					rdotf, Epotential);
			}
		}
		else
		{
			for (i = 0; i < Nm; i++)
			{
				LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nneig, NumNeig, radiusn, kijn, force, Ptensor,
					rdotf, Epotential);
				LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nextnneig, NumNextNeig, radiusnn, kijnn, force, Ptensor,
					rdotf, Epotential);
			}
		}

	}
	else
	{
		if (velPtensor)
		{
			Eigen::MatrixXd vtr(dim, Nm);
			vtr = (p_Lattice->Htph) * (p_Lattice->vtph);
			for (i = 0; i < Nm; i++)
			{
				Ptensor += (vtr.col(i))*(vtr.col(i).transpose());
				LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nneig, NumNeig, radiusn, kijn, force, Ptensor,
					rdotf, Epotential);
			}
		}
		else
		{
			for (i = 0; i < Nm; i++)
			{
				LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nneig, NumNeig, radiusn, kijn, force, Ptensor,
					rdotf, Epotential);
			}
		}
	}
}


template<class T>
void Elastic<T>::LocalEnergy(const Eigen::ArrayXi& spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, const Eigen::ArrayXXi & neighbours,
							const int & NumNeig, const Eigen::ArrayXd & radius, const double & kij, double & rdotf, double & Epotential)
{
	double modu;
	double requi;
	Eigen::VectorXd fij(dim);
	Eigen::VectorXd rij(dim);
	int j, k;
	for (j = 0; j < NumNeig / 2; j++)
	{
		k = neighbours(currMol, j);
		rij = rt.col(currMol) - rt.col(k);
		rij = rij.array() - (rij.array()).round();
		rij = Ht * rij;
		modu = rij.norm(); //modulus vector between balls
		requi = radiusn(spin(currMol)) + radiusn(spin(k));
		fij = (-kij * (modu - requi) / modu)*rij;
		rdotf += fij.dot(rij);
		Epotential += 0.5*kij*pow(modu - requi, 2.0);
	}
}

template<class T>
void Elastic<T>::LocalEnergy(const Eigen::ArrayXi& spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, const Eigen::ArrayXXi & neighbours, const int & NumNeig, const Eigen::ArrayXd & radius, const Eigen::ArrayXd & kij, double & rdotf, double & Epotential)
{
	double modu;
	double requi;
	Eigen::VectorXd fij(dim);
	Eigen::VectorXd rij(dim);
	int j, k;
	for (j = 0; j < NumNeig / 2; j++)
	{
		k = neighbours(currMol, j);
		rij = rt.col(currMol) - rt.col(k);
		rij = rij.array() - (rij.array()).round();
		rij = Ht * rij;
		modu = rij.norm(); //modulus vector between balls
		requi = radiusn(spin(currMol)) + radiusn(spin(k));
		fij = (-kij(j) * (modu - requi) / modu)*rij;
		rdotf += fij.dot(rij);
		Epotential += 0.5*kij(j)*pow(modu - requi, 2.0);
	}
}

//rdotf, and Epotential are set to zero before calculating
//here we use rt,and Ht, coordinates must be scaled
template<class T>
void Elastic<T>::CalculatePotentialEnergy(double & rdotf, double & Epotential)
{
	if (ActiveNextNearNeig)
	{
		rdotf = 0.0;
		Epotential = 0.0;
		int i;
		for (i = 0; i < Nm; i++)
		{
			LocalEnergy(p_Lattice->spin, p_Lattice->rt, p_Lattice->Ht, i, p_Lattice->nneig, NumNeig, radiusn, kijn, rdotf, Epotential);
			LocalEnergy(p_Lattice->spin, p_Lattice->rt, p_Lattice->Ht, i, p_Lattice->nextnneig, NumNextNeig, radiusnn, kijnn, rdotf, Epotential);
		}
		

	}
	else
	{
		rdotf = 0.0;
		Epotential = 0.0;
		int i;
		for (i = 0; i < Nm; i++)
		{
			LocalEnergy(p_Lattice->spin, p_Lattice->rt, p_Lattice->Ht, i, p_Lattice->nneig, NumNeig, radiusn, kijn, rdotf, Epotential);
		}
	}

}

//Bend potential
//====================================================================================================
Bend::Bend(Lattice * Lat, const double & in_ktheta, const double& in_ko)
{
	m_name = "Bend potential";
	p_Lattice = Lat;
	ktheta = in_ktheta;
	ko = in_ko;
	NumNeig = Lat->GetNumNeig();
	Nm = Lat->GetNm();
	dim = Lat->GetDim();
}

std::string Bend::GetName()
{
	return m_name;
}

void Bend::LocalForce(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, const Eigen::ArrayXXi & neighbours, 
					 Eigen::MatrixXd & force, double & Epotential, Eigen::MatrixXd& figroup, Eigen::MatrixXd& rigroup, Eigen::VectorXd& fisum)
{
	switch (dim)
	{
	case 2:
	{
		int k, n, j;
		Eigen::Vector2d rji, rki, ri, rj, rk;
		double rrji, rrki, rjirki;
		double thetaijk, fval;
		Eigen::Vector2d fi, fj, fk;
		for (n = 0; n < NumNeig; n++)
		{
			j = neighbours(currMol, n);
			k = neighbours(currMol, mod(n + 1, NumNeig));
			rj = rt.col(j);
			rk = rt.col(k);
			rji = rj - rt.col(currMol);
			rki = rk - rt.col(currMol);
			rigroup.col(n) = Ht * (rj - ((rji.array()).round()).matrix());
			rji = rji.array() - (rji.array()).round();
			rki = rki.array() - (rki.array()).round();
			rji = Ht * rji;
			rki = Ht * rki;
			rrji = rji.dot(rji);
			rrki = rki.dot(rki);
			rjirki = rji.dot(rki);
			thetaijk = acos(rjirki / (pow(rrji*rrki, 0.5)));
			fval = ktheta * (thetaijk - ko) / (sqrt(rrki*rrji - rjirki * rjirki));
			fi = fval * (rji*(rjirki / rrji - 1.0) + rki * (rjirki / rrki - 1.0));
			fj = fval * (rki - rji * (rjirki / rrji));
			fk = fval * (rji - rki * (rjirki / rrki));
			fisum += fi;
			figroup.col(n) += fj;
			figroup.col(mod(n + 1, NumNeig)) += fk;
			force.col(currMol) += fi;
			force.col(j) += fj;
			force.col(k) += fk;
			Epotential += 0.5*ktheta*pow(thetaijk - ko, 2.0);
		}
		break;
	}
	case 3:
	{
		int k, n, j;
		Eigen::Vector3d rji, rki, rj, rk;
		double rrji, rrki, rjirki;
		double thetaijk, fval;
		Eigen::Vector3d fi, fj, fk;
		for (n = 0; n < NumNeig; n++) {
			j = neighbours(currMol, n);
			rj = rt.col(j);
			rji = rj - rt.col(currMol);
			rigroup.col(n) = Ht * (rj - ((rji.array()).round()).matrix()); //get position of atom if in replica cell
			rji = rji.array() - (rji.array()).round();
			rji = Ht * rji;
			rrji = rji.dot(rji);
			//=====================================================
			k = neighbours(currMol, mod(n + 1, NumNeig));
			rk = rt.col(k);
			rki = rk - rt.col(currMol);
			rki = rki.array() - (rki.array()).round();
			rki = Ht * rki;
			rrki = rki.dot(rki);
			rjirki = rji.dot(rki);
			thetaijk = acos(rjirki / (pow(rrji*rrki, 0.5)));
			fval = ktheta * (thetaijk - ko) / (sqrt(rrki*rrji - rjirki * rjirki));
			fi = fval * (rji*(rjirki / rrji - 1.0) + rki * (rjirki / rrki - 1.0));
			fj = fval * (rki - rji * (rjirki / rrji));
			fk = fval * (rji - rki * (rjirki / rrki));
			fisum += fi;
			figroup.col(n) += fj;
			figroup.col(mod(n + 1, NumNeig)) += fk;
			force.col(currMol) += fi;
			force.col(j) += fj;
			force.col(k) += fk;
			Epotential += 0.5*ktheta*pow(thetaijk - ko, 2.0);
			//repeat for the other non 180 degrees
			k = neighbours(currMol, mod(n + 2, NumNeig));
			rk = rt.col(k);
			rki = rk - rt.col(currMol);
			rki = rki.array() - (rki.array()).round();
			rki = Ht * rki;
			rrki = rki.dot(rki);
			rjirki = rji.dot(rki);
			thetaijk = acos(rjirki / (pow(rrji*rrki, 0.5)));
			fval = ktheta * (thetaijk - ko) / (sqrt(rrki*rrji - rjirki * rjirki));
			fi = fval * (rji*(rjirki / rrji - 1.0) + rki * (rjirki / rrki - 1.0));
			fj = fval * (rki - rji * (rjirki / rrji));
			fk = fval * (rji - rki * (rjirki / rrki));
			fisum += fi;
			figroup.col(n) += fj;
			figroup.col(mod(n + 2, NumNeig)) += fk;
			force.col(currMol) += fi;
			force.col(j) += fj;
			force.col(k) += fk;
			Epotential += 0.5*ktheta*pow(thetaijk - ko, 2.0);
		}
	}
	}
	
}

void Bend::CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor, double & rdotf, double & Epotential, const bool & velPtensor)
{
	Eigen::MatrixXd figroup(dim, NumNeig);
	Eigen::MatrixXd rigroup(dim, NumNeig);
	Eigen::VectorXd fisum(dim);
	Eigen::VectorXd ri;
	Ptensor.setZero();
	force.setZero();
	rdotf = 0.0;
	Epotential = 0.0;
	int i, n;

	if (velPtensor) {
		Eigen::MatrixXd vtr(dim, Nm);
		vtr = (p_Lattice->Htph) * (p_Lattice->vtph); //unscale velocities
		for (i = 0; i < Nm; i++)
		{
			Ptensor += (vtr.col(i))*(vtr.col(i).transpose());
			rigroup.setZero();
			figroup.setZero();
			fisum.setZero();
			ri = (p_Lattice->Htph) * ((p_Lattice->rtph).col(i));

			LocalForce(p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nneig, force, Epotential, figroup, rigroup, fisum);

			Ptensor += fisum * (ri).transpose();
			rdotf += fisum.dot(ri);
			for (n = 0; n < NumNeig; n++) {
				Ptensor += figroup.col(n)*(rigroup.col(n)).transpose();
				rdotf += figroup.col(n).dot(rigroup.col(n));
			}
		}
	}
	else
	{
		for (i = 0; i < Nm; i++)
		{
			rigroup.setZero();
			figroup.setZero();
			fisum.setZero();
			ri = (p_Lattice->Htph) * ((p_Lattice->rtph).col(i));

			LocalForce(p_Lattice->rtph, p_Lattice->Htph, i, p_Lattice->nneig, force, Epotential, figroup, rigroup, fisum);

			Ptensor += fisum * (ri).transpose();
			rdotf += fisum.dot(ri);
			for (n = 0; n < NumNeig; n++) {
				Ptensor += figroup.col(n)*(rigroup.col(n)).transpose();
				rdotf += figroup.col(n).dot(rigroup.col(n));
			}
		}
	}
}

void Bend::LocalEnergy(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, const Eigen::ArrayXXi & neighbours, double & Epotential, Eigen::MatrixXd & figroup, Eigen::MatrixXd & rigroup, Eigen::VectorXd & fisum)
{
	switch (dim)
	{
	case 2:
	{
		int k, n, j;
		Eigen::Vector2d rji, rki, ri, rj, rk;
		double rrji, rrki, rjirki;
		double thetaijk, fval;
		Eigen::Vector2d fi, fj, fk;
		for (n = 0; n < NumNeig; n++)
		{
			j = neighbours(currMol, n);
			k = neighbours(currMol, mod(n + 1, NumNeig));
			rj = rt.col(j);
			rk = rt.col(k);
			rji = rj - rt.col(currMol);
			rki = rk - rt.col(currMol);
			rigroup.col(n) = Ht * (rj - ((rji.array()).round()).matrix());
			rji = rji.array() - (rji.array()).round();
			rki = rki.array() - (rki.array()).round();
			rji = Ht * rji;
			rki = Ht * rki;
			rrji = rji.dot(rji);
			rrki = rki.dot(rki);
			rjirki = rji.dot(rki);
			thetaijk = acos(rjirki / (pow(rrji*rrki, 0.5)));
			fval = ktheta * (thetaijk - ko) / (sqrt(rrki*rrji - rjirki * rjirki));
			fi = fval * (rji*(rjirki / rrji - 1.0) + rki * (rjirki / rrki - 1.0));
			fj = fval * (rki - rji * (rjirki / rrji));
			fk = fval * (rji - rki * (rjirki / rrki));
			fisum += fi;
			figroup.col(n) += fj;
			figroup.col(mod(n + 1, NumNeig)) += fk;
			Epotential += 0.5*ktheta*pow(thetaijk - ko, 2.0);
		}
		break;
	}
	case 3:
	{
		int k, n, j;
		Eigen::Vector3d rji, rki, rj, rk;
		double rrji, rrki, rjirki;
		double thetaijk, fval;
		Eigen::Vector3d fi, fj, fk;
		for (n = 0; n < NumNeig; n++) {
			j = neighbours(currMol, n);
			rj = rt.col(j);
			rji = rj - rt.col(currMol);
			rigroup.col(n) = Ht * (rj - ((rji.array()).round()).matrix()); //get position of atom if in replica cell
			rji = rji.array() - (rji.array()).round();
			rji = Ht * rji;
			rrji = rji.dot(rji);
			//=====================================================
			k = neighbours(currMol, mod(n + 1, NumNeig));
			rk = rt.col(k);
			rki = rk - rt.col(currMol);
			rki = rki.array() - (rki.array()).round();
			rki = Ht * rki;
			rrki = rki.dot(rki);
			rjirki = rji.dot(rki);
			thetaijk = acos(rjirki / (pow(rrji*rrki, 0.5)));
			fval = ktheta * (thetaijk - ko) / (sqrt(rrki*rrji - rjirki * rjirki));
			fi = fval * (rji*(rjirki / rrji - 1.0) + rki * (rjirki / rrki - 1.0));
			fj = fval * (rki - rji * (rjirki / rrji));
			fk = fval * (rji - rki * (rjirki / rrki));
			fisum += fi;
			figroup.col(n) += fj;
			figroup.col(mod(n + 1, NumNeig)) += fk;
			Epotential += 0.5*ktheta*pow(thetaijk - ko, 2.0);
			//repeat for the other non 180 degrees
			k = neighbours(currMol, mod(n + 2, NumNeig));
			rk = rt.col(k);
			rki = rk - rt.col(currMol);
			rki = rki.array() - (rki.array()).round();
			rki = Ht * rki;
			rrki = rki.dot(rki);
			rjirki = rji.dot(rki);
			thetaijk = acos(rjirki / (pow(rrji*rrki, 0.5)));
			fval = ktheta * (thetaijk - ko) / (sqrt(rrki*rrji - rjirki * rjirki));
			fi = fval * (rji*(rjirki / rrji - 1.0) + rki * (rjirki / rrki - 1.0));
			fj = fval * (rki - rji * (rjirki / rrji));
			fk = fval * (rji - rki * (rjirki / rrki));
			fisum += fi;
			figroup.col(n) += fj;
			figroup.col(mod(n + 2, NumNeig)) += fk;
			Epotential += 0.5*ktheta*pow(thetaijk - ko, 2.0);
		}
	}
	}

}

void Bend::CalculatePotentialEnergy(double & rdotf, double & Epotential)
{
	Eigen::MatrixXd figroup(dim, NumNeig);
	Eigen::MatrixXd rigroup(dim, NumNeig);
	Eigen::VectorXd fisum(dim);
	Eigen::VectorXd ri;
	rdotf = 0.0;
	Epotential = 0.0;
	int i, n;
	for (i = 0; i < Nm; i++)
	{
		rigroup.setZero();
		figroup.setZero();
		fisum.setZero();
		ri = (p_Lattice->Ht) * ((p_Lattice->rt).col(i));
		LocalEnergy(p_Lattice->rt, p_Lattice->Ht, i, p_Lattice->nneig, Epotential, figroup, rigroup, fisum);
		rdotf += fisum.dot(ri);
		for (n = 0; n < NumNeig; n++) {
			rdotf += figroup.col(n).dot(rigroup.col(n));
		}
	}
}

//Lennard-Jones potential
//==============================================================================================
LJ::LJ(Lattice * Lat, const double & in_rcut, const double & in_radiusn)
{
	p_Lattice = Lat;
	m_name = "Lennard-Jones Potential";
	radiusn = in_radiusn;
	rcut = in_rcut;

	//useful constants
	rci = 1.0 / rcut;
	rrcut = pow(rcut, 2.0);
	sigmafactor = pow(2.0, 1.0 / 6.0);
	
	Nm = Lat->GetNm();
	dim = Lat->GetDim();
	FirstCallMD = true; 
	FirstCallMC = true;  
	pointersMD.resize(Nm);
	pointersMC.resize(Nm+1);
	rsaveMD.resize(dim, Nm);
	rsaveMC.resize(dim, Nm);
	VerletListMD(Lat->rt, Lat->Ht); //initialize verlet list, pointers and rsave for MD
	VerletListMC(Lat->rt, Lat->Ht); //initialize verlet list, pointers and rsave for MC	
}

std::string LJ::GetName()
{
	return m_name;
}

void LJ::VerletListMD(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht)
{
	double r_list_factor = 1.2;
	double rl = r_list_factor * rcut;
	double drshell = rl - rcut;
	Eigen::ArrayXXd dr(dim, Nm);
	double maxdisp;
	int nl;
	double rlsq;
	int i, j, k;
	double rijsq;
	Eigen::VectorXd vrij(dim);
	double volume = abs((Ht.transpose()).determinant());
	//check if list needs reassignment
	//rsave are from last update
	//rl is the radius of the larger box
	if (FirstCallMD == false) {
		dr = Ht * rt - rsaveMD;
		//periodic boundary conditions
		//omit this since it is assumed that molecules won't leave the lattice
		//as it is a solid
		//dr.row(0)=dr.row(0)-(dr.row(0)).round();
		//dr.row(1)=dr.row(1)-(dr.row(1)).round();
		//dr.row(2)=dr.row(2)-(dr.row(2)).round();

		maxdisp = (dr.matrix().colwise().squaredNorm()).maxCoeff();
		//only update when the summ of the two largest displacements is greater
		//than rl, hence the 4 in the if statement
		if (4.0*maxdisp < drshell*drshell) {
			return; // don't reconstruct list of neighbours
		}
	}
	FirstCallMD = false;

	if (dim == 2) 
	{
		nl = (int)ceil((3.1416*Nm*(Nm - 1)*pow(rl, 2)) / (volume*2.0));
	}
	else if (dim == 3)
	{
		nl = (int)ceil((4 * 3.1416*Nm*(Nm - 1)*pow(rl, 3)) / (volume*6.0));
	}
	
	//std::cout << "yes\n";
	vlistMD.resize(nl);
	vlistMD.setZero();
	//cout<<"yes"<<endl;
	rlsq = rl * rl;
	k = -1;
	for (i = 0; i < Nm - 1; i++) {
		pointersMD(i) = k + 1;
		for (j = i + 1; j < Nm; j++) {
			vrij = rt.col(i) - rt.col(j);
			vrij = vrij.array() - (vrij.array()).round();
			vrij = Ht * vrij;
			rijsq = vrij.squaredNorm();
			if (rijsq < rlsq) {
				k = k + 1;
				if (k >= nl) {
					nl = (int)ceil(1.25*nl);
					vlistMD.conservativeResize(nl);
				}
				vlistMD(k) = j;
			}
		}
	}
	pointersMD(Nm - 1) = k + 1;
	rsaveMD = Ht * rt;
}

void LJ::VerletListMC(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht)
{
	//check if list needs reassignment
	  //xsave and ysave are from last update
	  //rl is the radius of the larger box
	double r_list_factor = 1.2;
	double rl = r_list_factor * rcut;
	double drshell = rl - rcut;
	Eigen::ArrayXXd dr(dim, Nm);
	double maxdisp;
	int nl;
	double rlsq;
	int i, j, k;
	double rijsq;
	Eigen::VectorXd vrij(dim);
	double volume = abs((Ht.transpose()).determinant());
	if (FirstCallMC == false) {
		dr = Ht * rt - rsaveMC;
		//periodic boundary conditions
		//dr.row(0)=dr.row(0)-(dr.row(0)/edgex).round()*edgex;
		//dr.row(1)=dr.row(1)-(dr.row(1)/edgey).round()*edgey;
		//dr.row(2)=dr.row(2)-(dr.row(2)/edgez).round()*edgez;

		maxdisp = (dr.matrix().colwise().squaredNorm()).maxCoeff();
		//only update when the summ of the two largest displacements is greater
		//than rl, hence the 4 in the if statement
		if (4.0*maxdisp < drshell*drshell) {
			return; // don't reconstruct list of neighbours
		}
	}
	FirstCallMC = false;
	if (dim == 2)
	{
		nl = (int)ceil((3.1416*Nm*(Nm - 1)*pow(rl, 2)) / (volume*2.0));
	}
	else if (dim == 3)
	{
		nl = (int)ceil((4 * 3.1416*Nm*(Nm - 1)*pow(rl, 3)) / (volume*6.0));
	}
	
	//std::cout << "yes\n";
	vlistMC.resize(nl);
	vlistMC.setZero();
	//std::cout<<"yes"<<endl;
	rlsq = rl * rl;
	k = -1;
	for (i = 0; i < Nm; i++) {
		pointersMC(i) = k + 1;
		for (j = 0; j < Nm; j++) {
			if (j != i) {
				vrij = rt.col(i) - rt.col(j);
				vrij = vrij.array() - (vrij.array()).round();
				vrij = Ht * vrij;
				rijsq = vrij.squaredNorm();
				if (rijsq < rlsq) {
					k = k + 1;
					if (k >= nl) {
						nl = (int)ceil(1.25*nl);
						vlistMC.conservativeResize(nl);
					}
					vlistMC(k) = j;
				}
			}
		}
	}
	pointersMC(Nm) = k + 1;
	rsaveMC = Ht * rt;
}

void LJ::LocalForce(const Eigen::ArrayXi & spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, Eigen::MatrixXd & force, double & Epotential, Eigen::MatrixXd & Ptensor, double & rdotf)
{
	double rr, ri; //r*r 1/(r*r)
	double fcval;
	double dVdr_rc, Vrc;
	double rij;
	int j, k;
	double rm;
	double sigma;
	Eigen::VectorXd vrij(dim);
	Eigen::VectorXd fij(dim);
	for (k = pointersMD(currMol); k <= pointersMD(currMol + 1) - 1; k++) {
		j = vlistMD(k);

		//periodic boundary conditions
		vrij = rt.col(currMol) - rt.col(j);
		vrij = vrij.array() - (vrij.array()).round();
		vrij = Ht * vrij;
		rr = vrij.squaredNorm();
		if (rr < rrcut) {
			rij = pow(rr, 0.5);
			rm = radiusn(spin(currMol)) + radiusn(spin(j));
			sigma = rm / sigmafactor;
			Vrc = 4.0*(pow(sigma*rci, 12.0) - pow(sigma*rci, 6.0));
			dVdr_rc = 48.0*(0.5*pow(sigma*rci, 8.0) - pow(sigma*rci, 14.0))*rcut / pow(sigma, 2.0);
			//dVdr_rc=0.0;
			ri = 1.0 / rij;
			fcval = 48.0*(pow(sigma*ri, 14.0) - 0.5*pow(sigma*ri, 8.0))*rij / pow(sigma, 2.0) + dVdr_rc;
			fij = (fcval / rij)*vrij;
			force.col(currMol) += fij;
			force.col(j) += (-1.0)*fij;
			Ptensor += vrij * fij.transpose();
			rdotf += fcval * rr / rij;
			Epotential += 4.0*(pow(sigma*ri, 12.0) - pow(sigma*ri, 6.0)) - Vrc - (rij - rcut)*dVdr_rc;
		}
	}
}

void LJ::CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor, double & rdotf, double & Epotential, const bool & velPtensor)
{
	int i;
	force.setZero();
	Ptensor.setZero();
	rdotf = 0.0;
	Epotential = 0.0;
	VerletListMD(p_Lattice->rt, p_Lattice->Ht);
	if (velPtensor)
	{
		Eigen::MatrixXd vtr(dim, Nm);
		vtr = (p_Lattice->Htph) * (p_Lattice->vtph); //unscale velocities
		Ptensor += (vtr.col(Nm-1))*(vtr.col(Nm-1).transpose());
		for (i = 0; i < Nm-1; i++)
		{
			Ptensor += (vtr.col(i))*(vtr.col(i).transpose());
			LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, force, Epotential, Ptensor, rdotf);
		}

	}
	else
	{
		for (i = 0; i < Nm - 1; i++)
		{
			LocalForce(p_Lattice->spin, p_Lattice->rtph, p_Lattice->Htph, i, force, Epotential, Ptensor, rdotf);
		}
	}
}

void LJ::LocalEnergy(const Eigen::ArrayXi & spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, double & Epotential, double & rdotf)
{
	double rr, ri; //r*r 1/(r*r)
	double fcval;
	double dVdr_rc, Vrc;
	double rij;
	int  j, k;
	double sigma;
	double rm;
	Eigen::VectorXd vrij(dim);
	for (k = pointersMD(currMol); k <= pointersMD(currMol + 1) - 1; k++) {
		j = vlistMD(k);
		//periodic boundary conditions
		vrij = rt.col(currMol) - rt.col(j);
		vrij = vrij.array() - (vrij.array()).round();
		vrij = Ht * vrij;
		rr = vrij.squaredNorm();
		if (rr < rrcut) {
			rij = pow(rr, 0.5);
			rm = radiusn(spin(currMol)) + radiusn(spin(j));
			sigma = rm / sigmafactor;
			Vrc = 4.0*(pow(sigma*rci, 12.0) - pow(sigma*rci, 6.0));
			dVdr_rc = 48.0*(0.5*pow(sigma*rci, 8.0) - pow(sigma*rci, 14.0))*rcut / pow(sigma, 2.0);
			ri = 1.0 / rij;
			fcval = 48.0*(pow(sigma*ri, 14.0) - 0.5*pow(sigma*ri, 8.0))*rij / pow(sigma, 2.0) + dVdr_rc;
			rdotf += fcval * rr / rij;
			Epotential += 4.0*(pow(sigma*ri, 12.0) - pow(sigma*ri, 6.0)) - Vrc - (rij - rcut)*dVdr_rc;
		}
	}
}

void LJ::CalculatePotentialEnergy(double & rdotf, double & Epotential)
{
	int i;
	rdotf = 0.0;
	Epotential = 0.0;
	for (i = 0; i < Nm - 1; i++)
	{
		LocalEnergy(p_Lattice->spin, p_Lattice->rt, p_Lattice->Ht, i, Epotential, rdotf);
	}
}