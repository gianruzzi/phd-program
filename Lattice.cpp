#include <iostream>
#include "dependecies/eigen3/Eigen/Dense"
#include <string>
#include <cmath>
#include <exception>
#include <random>
#include <memory>
#include "Lattice.h"
#include "Integrator.h"
#include "Potential.h"

int mod(int a, int b) {
	int res = a % b;
	if (res < 0) {
		res += b;
	}
	return res;
}

Lattice::Lattice(const int& nx, const int& ny, const int& nz, const double& density, std::string const& typeoflattice, 
	const double& temperature, std::string const& spinstate, const double& in_delta, const double& in_g)
{
	isCoorScaled = false;
	isVelScaled = false;
	Nx = nx;
	Ny = ny;
	Nz = nz;
	dim = 3;
	TotSpin = 0;
	Epotential = 0.0;
	Ekinetic = 0.0;
	Espin = 0.0;
	pressure = 0.0;
	InitTemperature = temperature;
	CurrTemperature = InitTemperature;
	g = in_g;
	delta = in_delta;
	Heff = 0.5*(delta - CurrTemperature * log(g));

	//First we need to set the type of lattice
	m_TypeOfLattice=Set3DType(typeoflattice);
	SetLattice(density);
	nf = dim * (Nm - 1); //we need to set the lattice first to set Nm
	volt = abs((Ht.transpose()).determinant());
	voltph = 0.0;

	//Init to zero r v a and H at time!=t (a for all t)
	SetNonCurrentMatricesToZero();
	Ptensor = Eigen::MatrixXd::Zero(3, 3);

	//Init velocities given the temperature
	Set3DVelocities();

	//Init spin state of the lattice
	m_initSpinState = TypeOfSpinState(spinstate);
	SetSpinState();
	Espin = Heff * TotSpin;

	//Init force matrix
	totalForces.resize(3, Nm);
	totalForces.setZero();

	rdotf = 0.0;

}
Lattice::Lattice(const int& nx, const int& ny, const double& density, std::string const& typeoflattice, 
	const double& temperature, std::string const& spinstate, const double& in_delta, const double& in_g)
{
	isCoorScaled = false;
	isVelScaled = false;
	Nx = nx;
	Ny = ny;
	Nz = 0;
	dim = 2;
	TotSpin = 0;
	Epotential = 0.0;
	Ekinetic = 0.0;
	pressure = 0.0;
	InitTemperature = temperature;
	CurrTemperature= InitTemperature;
	g = in_g;
	delta = in_delta;
	Heff = 0.5*(delta - CurrTemperature * log(g));

	//First we need to set the type of lattice
	m_TypeOfLattice = Set2DType(typeoflattice);
	SetLattice(density);
	nf = dim * (Nm - 1); //we need to set the lattice first to set Nm
	volt= abs((Ht.transpose()).determinant());
	voltph = 0.0;

	//Init to zero r v a and H at time!=t (a for all t)
	SetNonCurrentMatricesToZero();
	Ptensor = Eigen::MatrixXd::Zero(2, 2);

	//Init velocities given the temperature
	Set2DVelocities();

	//Init spin state of the lattice
	m_initSpinState = TypeOfSpinState(spinstate);
	SetSpinState();
	Espin = Heff * TotSpin;

	//Init force matrix
	totalForces.resize(2, Nm);
	totalForces.setZero();

	rdotf = 0.0;

}

//Functions===============================================

int Lattice::GetNx() { return Nx; }
int Lattice::GetNy() { return Ny; }
int Lattice::GetNz() { return Nz; }
int Lattice::GetNm() { return Nm; }
int Lattice::GetNf() { return nf; }
int Lattice::GetDim() { return dim; }
int Lattice::GetNumNeig() { return NumNeig; }
int Lattice::GetNumNextNeig() { return NumNextNeig; }
double Lattice::GetInitTemperaure() { return InitTemperature; }

void Lattice::ChangeCurrTemperature(const double& NewTemperature)
{
	CurrTemperature = NewTemperature;
	Heff = 0.5*(delta - CurrTemperature * log(g));
	Espin = Heff * TotSpin;
}

double Lattice::GetHeff() { return Heff; }
double Lattice::Getg() { return g; }
double Lattice::GetDelta() { return delta; }

/*Returns type of lattice used as a string*/
std::string Lattice::GetTypeOfLattice()
{
	switch (m_TypeOfLattice)
	{
	case square:
		return "square";
		break;
	case triangular:
		return "triangular";
		break;
	case kagome:
		return "kagome";
		break;
	case cubic:
		return "cubic";
		break;
	case fcc:
		return "fcc";
		break;
	default:
		return "Error Lattice Type";
		break;
	}
}

/*Returns initial Spin state*/
std::string Lattice::GetInitSpinState()
{

	switch (m_initSpinState)
	{
	case LS:
		return "LS";
		break;
	case HS:
		return "HS";
		break;
	case Random:
		return "Random";
		break;
	default:
		return "Error Spin State";
		break;
	}
}

/*Resizes the matrices at initialization once Nm and type of lattice has been set*/
void Lattice::SetMatricesSizes()
{
	rt.resize(dim, Nm);
	rtph.resize(dim, Nm);
	vt.resize(dim, Nm);
	vtph.resize(dim, Nm);
	atmh.resize(dim, Nm);
	atm2h.resize(dim, Nm);
	at.resize(dim, Nm);
	atph.resize(dim, Nm);
	nneig.resize(Nm,NumNeig);
	nextnneig.resize(Nm,NumNextNeig);
	Ht.resize(dim,dim);
	Htph.resize(dim,dim);
	Ptensor.resize(dim, dim);
	spin.resize(Nm);
}

/*Set the matrices for H, r, v, and a to zero for time!=t (a for all t)*/
void Lattice::SetNonCurrentMatricesToZero()
{
	rtph.setZero();
	vtph.setZero();
	atmh.setZero();
	atm2h.setZero();
	at.setZero();
	atph.setZero();
	Htph.setZero();
}


//Spin functions========================================================================
/*returns a SpinState variable from input text*/
Lattice::SpinState Lattice::TypeOfSpinState(std::string const& spinstate)
{
	if (spinstate == "LS") { return LS; }
	else if (spinstate == "HS") { return HS; }
	else if (spinstate == "Random") { return Random; }
	else {
		std::cout << "Invalid Spin State\n";
		//throw std::exception();
	}
}

/*Sets the initial spin state of the lattice*/
void Lattice::SetSpinState()
{
	unsigned iseed = 12845;
	std::default_random_engine generator(iseed);
	std::uniform_int_distribution<int> distributionspin(0, 1);
	switch (m_initSpinState)
	{
	case LS:
		for (int i = 0; i < Nm; i++) {
			spin(i) = 0;
			TotSpin += -1;
		}
		break;
	case HS:
		for (int i = 0; i < Nm; i++) {
			spin(i) = 1;
			TotSpin += 1;
		}
		break;
	case Random:
		for (int i = 0; i < Nm; i++) {
			spin(i)=distributionspin(generator);
			if (spin(i) == 1) {
				TotSpin += 1;
			}
			else if (spin(i) == 0) {
				TotSpin += -1;
			}
		}
		break;
	default:
		break;
	}
}

//functions to set initial velocities===================================================
/*Sets the velocities of the particles with random orientation but a given module 2D case*/
void Lattice::Set2DVelocities()
{
	double mypi = 3.14159265358;
	unsigned iseed = 12845;
	std::default_random_engine generator(iseed);
	std::uniform_real_distribution<double> distributionr(0.0, 1.0);
	double s;
	double vxsum = 0.0;
	double vysum = 0.0;
	double velmag;
	double vvsum = 0.0;
	for (int i = 0; i < Nm; i++) {
		s = 2.0*mypi*distributionr(generator);
		vt(0, i) = cos(s);
		vt(1, i) = sin(s);
		vxsum += vt(0, i);
		vysum += vt(1, i);
	}
	vt.row(0) = vt.row(0).array() - vxsum / Nm;
	vt.row(1) = vt.row(1).array() - vysum / Nm;
	vvsum = vt.row(0).squaredNorm() + vt.row(1).squaredNorm();
	velmag = sqrt(nf*InitTemperature / (m*vvsum));
	vt = vt * velmag;

}
/*Sets the velocities of the particles with random orientation but a given module 3D case*/
void Lattice::Set3DVelocities()
{
	double mypi = 3.14159265358;
	unsigned iseed = 12845;
	std::default_random_engine generator(iseed);
	std::uniform_real_distribution<double> distributionr(0.0, 1.0);
	double s, x, y;
	double vxsum = 0.0;
	double vysum = 0.0;
	double vzsum = 0.0;
	double velmag;
	double vvsum = 0.0;
	for (int i = 0; i < Nm; i++) {
		s = 2.0;
		while (s > 1.0) {
			x = 2.0*distributionr(generator) - 1.0;
			y = 2.0*distributionr(generator) - 1.0;
			s = pow(x, 2.0) + pow(y, 2.0);
		}
		vt(2, i) = (1.0 - 2.0*s);
		s = 2.0*pow(1.0 - s, 0.5);
		vt(0, i) = s * x;
		vt(1, i) = s * y;
		vxsum += vt(0, i);
		vysum += vt(1, i);
		vzsum += vt(2, i);
	}
	vt.row(0) = vt.row(0).array() - vxsum / Nm;
	vt.row(1) = vt.row(1).array() - vysum / Nm;
	vt.row(2) = vt.row(2).array() - vzsum / Nm;
	vvsum = vt.row(0).squaredNorm() + vt.row(1).squaredNorm()
		+ vt.row(2).squaredNorm();
	velmag = sqrt(nf*InitTemperature / (m*vvsum));
	vt = vt * velmag;
}

//Lattice Symmetry creators==============================================================
Lattice::TypeOfLattice Lattice::Set3DType(std::string const& typeoflattice)
{
	if (typeoflattice == "cubic") { return cubic; }
	else if (typeoflattice == "fcc") { return fcc; }
	else {
		std::cout << "Invalid Lattice\n";
		//throw std::exception();
	}
}

Lattice::TypeOfLattice Lattice::Set2DType(std::string const& typeoflattice)
{
	if (typeoflattice == "square") { return square; }
	else if (typeoflattice == "triangular") { return triangular; }
	else if (typeoflattice == "kagome") { return kagome; }
	else {
		std::cout << "Invalid Lattice\n";
		//throw std::exception();
	}
}

void Lattice::SetLattice(const double& density)
{
	switch (m_TypeOfLattice)
	{
	case square:
		SquareLattice(density);
		break;
	case triangular:
		TriangularLattice(density);
		break;
	case kagome:
		KagomeLattice(density);
		break;
	case cubic:
		CubicLattice(density);
		break;
	case fcc:
		FccLattice(density);
		break;
	default:
		break;
	}
}
/*Set positions of molecules in a square lattice centred at the origin*/
void Lattice::SquareLattice(const double& density)
{
	int i, j;
	double stepx, stepy;
	double dl;
	dl = sqrt(1.0 / density);
	Eigen::Vector2d c1(dl, 0.0);
	Eigen::Vector2d c2(0.0, dl);
	stepx = 0.5;
	stepy = double(Nx) - 0.5;
	Nm = Nx * Nx;
	NumNeig = 4;
	NumNextNeig = 4;
	SetMatricesSizes();
	for (i = 0; i < Nx; i++) {
		for (j = 0; j < Ny; j++) {
			//initial positions
			rt.col(j*Nx + i) = ((-stepx + i)*c1) + ((stepy - j)*c2);
			//nearest neighbours
			//uper part
			//    (1) o
			//        |
			// (0) o--o
			nneig(j*Nx + i, 0) = j * Nx + mod(i - 1, Nx);
			nneig(j*Nx + i, 1) = mod(j - 1, Ny)*Nx + i;
			//lower part
			//   o--o (2)
			//   |
			//  o (3)
			nneig(j*Nx + i, 2) = j * Nx + mod(i + 1, Nx);
			nneig(j*Nx + i, 3) = mod(j + 1, Ny)*Nx + i;
			//next nearest neighbours, start up left and go clockwise
			//upper part
			// (0) o   o (1)
			//      \ /
			//       o
			nextnneig(j*Nx + i, 0) = mod(j - 1, Ny)*Nx + mod(i - 1, Nx);
			nextnneig(j*Nx + i, 1) = mod(j - 1, Ny)*Nx + mod(i + 1, Nx);
			//lower part
			//       o
			//     /  \
            //(3) o    o (2)
			nextnneig(j*Nx + i, 2) = mod(j + 1, Ny)*Nx + mod(i + 1, Nx);
			nextnneig(j*Nx + i, 3) = mod(j + 1, Ny)*Nx + mod(i - 1, Nx);
		}
	}
	Ht << c1 * double(Nx), c2*double(Nx);
}

/*Set positions of molecules in a triangular lattice centred at the origin*/
void Lattice::TriangularLattice(const double& density)
{
	int i, j;
	double stepx, stepy;
	double a=sqrt(2.0/(sqrt(3.0)*density));
	Eigen::Vector2d b1(a, 0.0);
	Eigen::Vector2d b2(0.0, a / 2.0*sqrt(3.0));
	stepx = (double(Nx) / 2.0) - 0.5;
	stepy = (double(Ny) / 2.0) - 0.5;
	Nm = Nx * Ny;
	NumNeig = 6;
	NumNextNeig = 1;
	SetMatricesSizes();
	for (i = 0; i < Nx; i++) {
		for (j = 0; j < Ny; j++) {
			//initial positions
			rt.col(j*Nx + i) = ((-stepx + i)*b1 + b1 * pow(-1, j)*0.25) + ((stepy - j)*b2);
			//nearest neighbours
			//uper part
			//    (1)o    o (3)
			//        \ /
			// (0) o-- o
			nneig(j*Nx + i, 0) = j * Nx + mod(i - 1, Nx);
			if (j % 2 == 0) {
				nneig(j*Nx + i, 1) = mod(j - 1, Ny)*Nx + i;
				nneig(j*Nx + i, 2) = mod(j - 1, Ny)*Nx + mod(i + 1, Nx);
			}
			else {
				nneig(j*Nx + i, 1) = mod(j - 1, Ny)*Nx + mod(i - 1, Nx);
				nneig(j*Nx + i, 2) = mod(j - 1, Ny)*Nx + i;
			}
			//lower part
			//       o--o (4)
			//      / \
            // (6)o    o (5)
			nneig(j*Nx + i, 3) = j * Nx + mod(i + 1, Nx);
			if (j % 2 == 0) {
				nneig(j*Nx + i, 4) = mod(j + 1, Ny)*Nx + mod(i + 1, Nx);
				nneig(j*Nx + i, 5) = mod(j + 1, Ny)*Nx + i;
			}
			else {
				nneig(j*Nx + i, 4) = mod(j + 1, Ny)*Nx + i;
				nneig(j*Nx + i, 5) = mod(j + 1, Ny)*Nx + mod(i - 1, Nx);
			}

		}
	}
	nextnneig.setZero(); //change when know what are the next nearest neighbours
	Ht << b1 * double(Nx), b2*double(Ny);
}

/*Set positions of molecules in a kagome lattice centred at the origin*/
void Lattice::KagomeLattice(const double& density)
{
	int i, j, index;
	double stepx, stepy;
	double a=sqrt(3.0/(2.0*density*sqrt(3.0)));
	Eigen::Vector2d c1(2.0*a, 0.0);
	Eigen::Vector2d c2(a, sqrt(3.0)*a);
	Eigen::Vector2d a1(a, 0.0);
	Eigen::Vector2d a2(a / 2.0, sqrt(3.0)*a / 2.0);
	stepx = (double(Nx) / 2.0) - 0.25;
	stepy = (double(Ny) / 2.0) - 0.25;
	index = 0;
	Nm = 3 * Nx*Ny;
	NumNeig = 1; //change when know what the nearest neighbours are
	NumNextNeig = 1; //change when know what the next nearest neighbours are
	SetMatricesSizes();
	for (i = 0; i < Ny; i++) {
		for (j = 0; j < Nx; j++) {
			//initial positions
			rt.col(index) = ((-stepx + j)*c1) + ((-stepy + i)*c2);
			rt.col(index + 1) = rt.col(index) + a1;
			rt.col(index + 2) = rt.col(index) + a2;
			index += 3;
		}
	}
	nneig.setZero(); //change when know what the nearest neighbours are
	nextnneig.setZero(); //change when know what the next nearest neighbours are
	Ht << c1 * double(Nx), c2*double(Ny);
}

/*Set positions of molecules in a cubic lattice centred at the origin*/
void Lattice::CubicLattice(const double& density)
{
	int i, j, k, index;
	double stepx, stepy, stepz;
	double dl;
	dl = pow(1.0 / density, 1.0 / 3.0);
	Eigen::Vector3d c1(dl, 0.0, 0.0);
	Eigen::Vector3d c2(0.0, dl, 0.0);
	Eigen::Vector3d c3(0.0, 0.0, dl);
	stepx = (double(Nx) / 2.0) - 0.5;
	stepy = (double(Nx) / 2.0) - 0.5;
	stepz = (double(Nx) / 2.0) - 0.5;
	Nm = Nx * Nx*Nx;
	NumNeig = 6;
	NumNextNeig = 12;
	SetMatricesSizes();
	for (i = 0; i < Nx; i++) {
		for (j = 0; j < Nz; j++) {
			for (k = 0; k < Ny; k++) {
				index = i * Ny*Nz + j * Ny + k;
				rt.col(index) = (stepx - i)*c1 + (-stepy + k)*c2 + (stepz - j)*c3;

				//upper part
				//       (1)  (2)
				//        |  /
				//  (0)--
				nneig(index, 0) = mod(k - 1, Ny) + j * Ny + i * Ny*Nz;
				nneig(index, 1) = i * Ny*Nz + mod(j - 1, Nz)*Ny + k;
				nneig(index, 2) = mod(i + 1, Nx)*Ny*Nz + j * Ny + k;
				//lower part
				//       --(3)
				//   /  |
				// (5) (4)
				nneig(index, 3) = mod(k + 1, Ny) + j * Ny + i * Ny*Nz;
				nneig(index, 4) = i * Ny*Nz + mod(j + 1, Nz)*Ny + k;
				nneig(index, 5) = mod(i - 1, Nx)*Ny*Nz + j * Ny + k;

				//next nearest neighbours
				nextnneig(index, 0) = i * Ny*Nz + mod(j - 1, Nz)*Ny + mod(k - 1, Ny); //left top zy plane
				nextnneig(index, 1) = i * Ny*Nz + mod(j - 1, Nz)*Ny + mod(k + 1, Ny); //right top zy plane
				nextnneig(index, 2) = mod(i + 1, Nx)*Ny*Nz + j * Ny + mod(k + 1, Ny);//back right xy plane
				nextnneig(index, 3) = mod(i + 1, Nx)*Ny*Nz + j * Ny + mod(k - 1, Ny);//back left xy plane
				nextnneig(index, 4) = mod(i - 1, Nx)*Ny*Nz + mod(j - 1, Nz)*Ny + k;//up front xz plane
				nextnneig(index, 5) = mod(i + 1, Nx)*Ny*Nz + mod(j - 1, Nz)*Ny + k;//up back xz plane

				nextnneig(index, 6) = i * Ny*Nz + mod(j + 1, Nz)*Ny + mod(k + 1, Ny); //low right zy plane
				nextnneig(index, 7) = i * Ny*Nz + mod(j + 1, Nz)*Ny + mod(k - 1, Ny); // low left zy plane
				nextnneig(index, 8) = mod(i - 1, Nx)*Ny*Nz + mod(j + 1, Nz)*Ny + k;//low front xz plane
				nextnneig(index, 9) = mod(i + 1, Nx)*Ny*Nz + mod(j + 1, Nz)*Ny + k;//low back  xz plane
				nextnneig(index, 10) = mod(i - 1, Nx)*Ny*Nz + j * Ny + mod(k + 1, Ny);//front right xy plane
				nextnneig(index, 11) = mod(i - 1, Nx)*Ny*Nz + j * Ny + mod(k - 1, Ny);//front left xy plane	
			}
		}
	}
	Ht << c1 * double(Nx), c2*double(Nx), c3*double(Nz);

}

/*Set positions of molecules in a FCC lattice centred at the origin*/
void Lattice::FccLattice(const double& density)
{
	int nx, ny, nz, j, n;
	double uvol;
	Eigen::Vector3d c, gap;
	Eigen::Vector3d region;
	uvol = 1.0 / pow(density / 4.0, 1.0 / 3.0);
	region << uvol * Nx, uvol*Nx, uvol*Nx;
	//cout << region << endl;
	gap << uvol, uvol, uvol;
	n = 0;
	Nm = 4 * Nx*Nx*Nx;
	NumNeig = 1; //change when know what the nearest neighbours are
	NumNextNeig = 1; //change when know what the next nearest neighbours are
	SetMatricesSizes();
	for (nz = 0; nz < Nz; nz++) {
		for (ny = 0; ny < Ny; ny++) {
			for (nx = 0; nx < Nx; nx++) {
				c(0) = nx + 0.25;
				c(1) = ny + 0.25;
				c(2) = nz + 0.25;
				c = c.array()*gap.array();
				c = c + (-0.5)*region;
				for (j = 0; j < 4; j++) {
					rt(0, n) = c(0);
					rt(1, n) = c(1);
					rt(2, n) = c(2);
					if (j != 3) {
						if (j != 0) {
							rt(0, n) += 0.5*gap(0);
						}
						if (j != 1) {
							rt(1, n) += 0.5*gap(1);
						}
						if (j != 2) {
							rt(2, n) += 0.5*gap(2);
						}
					}
					n += 1;
				}
			}
		}
	}
	nneig.setZero(); //change when know what the nearest neighbours are
	nextnneig.setZero(); //change when know what the next nearest neighbours are
	Ht << uvol * double(Nx), 0.0, 0.0,
		0.0, uvol*double(Nx), 0.0,
		0.0, 0.0, uvol*double(Nx);
}

//Integrator Functions
//=====================================================================================
void Lattice::PredictorStepCoordinates(const double& dt)
{
	PredictorStep(rtph, rt, vtph, vt, at, atmh, atm2h, dt);
}
void Lattice::CorrectorStepCoordinates(const double& dt)
{
	CorrectorStep(rtph, rt, vt, vtph, atph, at, atmh, dt);
}

//Structural functions
//======================================================================================
void Lattice::ScaleCoor()
{
	if (isCoorScaled==false)
	{
		rt = (Ht.inverse())*rt;
		isCoorScaled = true;
	}
}
void Lattice::ScaleVel()
{
	if (isVelScaled == false)
	{
		vt = (Ht.inverse())*vt;
		isVelScaled = true;
	}
}
void Lattice::UnscaleCoor()
{
	if (isCoorScaled == true)
	{
		rt = Ht*rt;
		isCoorScaled = false;
	}
}
void Lattice::UnscaleVel()
{
	if (isVelScaled == true)
	{
		vt = Ht*vt;
		isVelScaled = false;
	}
}

//coordinates must be scaled
void Lattice::WrapCoort()
{
	rt = (rt.array()) - (rt.array()).round();
}

//coordinated must be scaled
void Lattice::WrapCoortph()
{
	rtph = (rtph.array()) - (rtph.array()).round();
}

//Potential functions
//======================================================================
void Lattice::SetPotential(std::shared_ptr<iPotential> p_potential)
{
	if (!p_firstpotential)
	{
		p_firstpotential = p_potential;
	}
	else if (!p_secondpotential)
	{
		p_secondpotential = p_potential;
	}
	else if (!p_thirdpotential)
	{
		p_thirdpotential = p_potential;
	}
	else
	{
		std::cout << "Maximum number of potentails set\n";
	}
}

//Calculate total forces, max 3 potentials, if not assigned then pointer should be Null (false)
void Lattice::CalculateTotalForces()
{
	Eigen::MatrixXd tempForces(dim, Nm);
	Eigen::MatrixXd tempPtensor(dim, dim);
	double temprdotf=0.0;
	double tempEpotential = 0.0;
	bool velPtensor = true;
	totalForces.setZero();
	Ptensor.setZero();
	Epotential = 0.0;
	rdotf = 0.0;
	if (p_firstpotential)
	{
		p_firstpotential->CalculateForces(tempForces, tempPtensor, temprdotf, tempEpotential, velPtensor);
		velPtensor = false;
		totalForces += tempForces;
		Ptensor += tempPtensor;
		Epotential += tempEpotential;
		rdotf += temprdotf;
	}
	if (p_secondpotential)
	{
		p_secondpotential->CalculateForces(tempForces, tempPtensor, temprdotf, tempEpotential, velPtensor);
		velPtensor = false;
		totalForces += tempForces;
		Ptensor += tempPtensor;
		Epotential += tempEpotential;
		rdotf += temprdotf;
	}
	if (p_thirdpotential)
	{
		p_thirdpotential->CalculateForces(tempForces, tempPtensor, temprdotf, tempEpotential, velPtensor);
		velPtensor = false;
		totalForces += tempForces;
		Ptensor += tempPtensor;
		Epotential += tempEpotential;
		rdotf += temprdotf;
	}
}