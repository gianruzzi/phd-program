#include <iostream>
#include "dependecies/eigen3/Eigen/Dense"
#include <omp.h>
#include "Lattice.h"
#include "Potential.h"
#include "Integrator.h"


using namespace Eigen;
using namespace std;
int main() 
{
	//Lattice Lat2D(4, 4, 1.0, "square", 1.0,"LS");
	//cout << Lat2D.Ht << endl<<endl;
	//cout << Lat2D.nneig << endl<<endl;
	//cout << Lat2D.spin << endl<<endl;

	Lattice Lat3D(3, 3, 3, 1.0, "cubic", 1.0,"Random",1.0,365);
	
	cout << Lat3D.rt << endl << endl;
	cout << Lat3D.nneig.transpose() << endl << endl;
	cout << Lat3D.spin.transpose() << endl << endl;
	cout << Lat3D.Espin << endl << endl;
	cout << "Nx " << Lat3D.GetNx() << endl << endl;
	cout << "Spin State " << Lat3D.GetInitSpinState() << endl << endl;
	cout << "Lattice " << Lat3D.GetTypeOfLattice() << endl << endl;
	cout << "nneig " << Lat3D.GetNumNeig() << endl << endl;
	cout << "nnn " << Lat3D.GetNumNextNeig() << endl << endl;
	cout << "g " << Lat3D.Getg() << endl<<endl;
	cout << "Heff " << Lat3D.GetHeff() << endl;
	Lat3D.ChangeCurrTemperature(9);
	cout << "Heff " << Lat3D.GetHeff() << endl<<endl;
	Lat3D.ScaleCoor();
	Eigen::ArrayXd radiusn(2);
	radiusn << 1, 1;
	Eigen::ArrayXd radiusnn(2);
	radiusnn << 1, 1;
	double kijn = 2.0;
	double kijnn = 2.0;
	bool ActiveNext = true;
	Elastic<double> Elasticpot(&Lat3D, kijn, kijnn, radiusn, radiusnn, ActiveNext);
	//Bend bend1(&Lat3D, kijn, kijnn);
	//cout << "Name Pot " << Elasticpot.GetName() << endl<<endl;
	cin.get();
}