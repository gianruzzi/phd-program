#pragma once
#include "dependecies/eigen3/Eigen/Dense"

void PR4(Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& vt,
	const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh, const Eigen::MatrixXd& atm2h,
	const double& dt);

void PV4(Eigen::MatrixXd& vtph, const Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt,
	const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh, const Eigen::MatrixXd& atm2h,
	const double& dt);

void CR4(Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& vt,
	const Eigen::MatrixXd& atph, const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh,
	const double& dt);

void CV4(Eigen::MatrixXd& vtph, const Eigen::MatrixXd& rtph, const Eigen::MatrixXd& rt,
	const Eigen::MatrixXd& atph, const Eigen::MatrixXd& at, const Eigen::MatrixXd& atmh,
	const double& dt);


void PredictorStep(Eigen::MatrixXd& rtph, Eigen::MatrixXd& rt, Eigen::MatrixXd& vtph, Eigen::MatrixXd& vt,
	Eigen::MatrixXd& at, Eigen::MatrixXd& atmh, Eigen::MatrixXd& atm2h, const double& dt);

//each matrix contains the information for all particles and coordinates
//rows particles, columns coordinate
void CorrectorStep(Eigen::MatrixXd& rtph, Eigen::MatrixXd& rt, Eigen::MatrixXd& vt, Eigen::MatrixXd& vtph,
	Eigen::MatrixXd& atph, Eigen::MatrixXd& at, Eigen::MatrixXd& atmh, const double& dt);

//when using doubles
//====================================================================================================

void PR4(double& rtph, const double& rt, const double& vt,
	const double& at, const double& atmh, const double& atm2h,
	const double& dt);

void PV4(double& vtph, const double& rtph, const double& rt,
	const double& at, const double& atmh, const double& atm2h,
	const double& dt);

void CR4(double& rtph, const double& rt, const double& vt,
	const double& atph, const double& at, const double& atmh,
	const double& dt);

void CV4(double& vtph, const double& rtph, const double& rt,
	const double& atph, const double& at, const double& atmh,
	const double& dt);

void PredictorStep(double& rtph, double& rt, double& vtph, double& vt,
	double& at, double& atmh, double& atm2h, const double& dt);

void CorrectorStep(double& rtph, double& rt, double& vt, double& vtph,
	double& atph, double& at, double& atmh, const double& dt);