//Here are defined the potentials used
#pragma once
#include <string>
#include "dependecies/eigen3/Eigen/Dense"
#include "Lattice.h"
//#include "iPotential.h"


//Elastic potential
//==========================================================================================

template<class T>
class Elastic : public iPotential
{
private:
	std::string m_name;
	Lattice* p_Lattice;
	T kijn; //spring constants for nearest neighbours
	T kijnn; //spring constants for next nearest neigbours
	Eigen::ArrayXd radiusn; //Equilibrium radius nearest neighbours
	Eigen::ArrayXd radiusnn; //Equilibrium radius next nearest neighbours
	bool ActiveNextNearNeig; //flag to activate or desactivate next nearest neighbours, true:active
	int Nm;
	int NumNeig; //number of nearest neighbours
	int NumNextNeig; //number of next nearest neighbours
	int dim;//dimension of lattice
public:
	Elastic(Lattice* Lat, const T& in_kijn, const T& in_kijnn, const Eigen::ArrayXd& in_radiusn, const Eigen::ArrayXd& in_radiusnn, const bool& ActiveNext);
	std::string GetName();
	//velPtensor, indicate if we want to compute the velocity part of the Ptensor
	//if false do not compute it
	void CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor,
		double & rdotf, double & Epotential, const bool& velPtensor);
	//calculate force for a given molecule
	void LocalForce(const Eigen::ArrayXi& spin, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& Ht, const int& currMol, const Eigen::ArrayXXi& neighbours,
					const int& NumNeig, const Eigen::ArrayXd& radius, const double& kij, Eigen::MatrixXd& force,
					Eigen::MatrixXd& Ptensor, double& rdotf, double& Epotential);

	void LocalForce(const Eigen::ArrayXi& spin, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& Ht, const int& currMol, const Eigen::ArrayXXi& neighbours,
					const int& NumNeig, const Eigen::ArrayXd& radius, const Eigen::ArrayXd& kij, Eigen::MatrixXd& force,
					Eigen::MatrixXd& Ptensor, double& rdotf, double& Epotential);

	//calculate potential energy
	void CalculatePotentialEnergy(double & rdotf, double & Epotential);

	//calculate energy for a given molecule
	void LocalEnergy(const Eigen::ArrayXi& spin, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& Ht, const int& currMol, const Eigen::ArrayXXi& neighbours,
		const int& NumNeig, const Eigen::ArrayXd& radius, const double& kij, double& rdotf, double& Epotential);

	void LocalEnergy(const Eigen::ArrayXi& spin, const Eigen::MatrixXd& rt, const Eigen::MatrixXd& Ht, const int& currMol, const Eigen::ArrayXXi& neighbours,
		const int& NumNeig, const Eigen::ArrayXd& radius, const Eigen::ArrayXd& kij, double& rdotf, double& Epotential);
};

//Bend potential (non linear nearest neighbour angles)
//=================================================================================================================
class Bend : public iPotential
{
private:
	std::string m_name;
	Lattice* p_Lattice;
	double ktheta; //spring constants for nearest neighbours
	double ko; //equilibrium angle
	int Nm; //number of molecules
	int NumNeig; //number of nearest neighbours
	int dim;//dimension of lattice
public:
	Bend(Lattice* Lat, const double& in_ktheta, const double& in_ko);
	std::string GetName();
	void CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor, double & rdotf, double & Epotential, const bool& velPtensor);
	void LocalForce(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, const Eigen::ArrayXXi & neighbours,
					Eigen::MatrixXd & force, double & Epotential, Eigen::MatrixXd& figroup, Eigen::MatrixXd& rigroup, Eigen::VectorXd& fisum);

	void CalculatePotentialEnergy(double & rdotf, double & Epotential);
	void LocalEnergy(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, const Eigen::ArrayXXi & neighbours,
		double & Epotential, Eigen::MatrixXd& figroup, Eigen::MatrixXd& rigroup, Eigen::VectorXd& fisum);

};

//Lennard-Jones potential (non linear nearest neighbour angles)
//=================================================================================================================
class LJ : public iPotential
{
private:
	std::string m_name;
	Lattice* p_Lattice;
	Eigen::ArrayXd radiusn;
	double rcut;
	double rrcut; //rcut^2
	double rci; //1/rcut
	double sigmafactor;
	int Nm; //number of molecules
	int dim;//dimension of lattice
	Eigen::ArrayXi vlistMD; //verlet list, molecules inside radius
	Eigen::ArrayXi pointersMD; //pointers for vlist
	Eigen::MatrixXd rsaveMD;//hold r values from last update

	Eigen::ArrayXi vlistMC; //verlet list, molecules inside radius
	Eigen::ArrayXi pointersMC; //pointers for vlist
	Eigen::MatrixXd rsaveMC;//hold r values from last update

	bool FirstCallMD; //used to update verlet list in MD simulations 
	bool FirstCallMC; //used to update verlet list in MC simulations 
public:
	LJ(Lattice* Lat, const double& in_rcut, const double& in_radiusn);
	std::string GetName();

	//create the verlet list for neighbour molecules for MD calculations
	void VerletListMD(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht);

	//create the verlet list for neighbour molecules for MC calculations
	//different from MD as we have to get the neighbours of every particle
	void VerletListMC(const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht);

	void CalculateForces(Eigen::MatrixXd & force, Eigen::MatrixXd & Ptensor, double & rdotf, double & Epotential, const bool& velPtensor);
	void LocalForce(const Eigen::ArrayXi& spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol, 
					Eigen::MatrixXd & force, double & Epotential, Eigen::MatrixXd & Ptensor, double & rdotf);

	void CalculatePotentialEnergy(double & rdotf, double & Epotential);
	void LocalEnergy(const Eigen::ArrayXi& spin, const Eigen::MatrixXd & rt, const Eigen::MatrixXd & Ht, const int & currMol,
		double & Epotential, double & rdotf);

};
